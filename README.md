# Parking Lot #

A Parking Lot application that stores predetermined amount of Cars. The application implements several Parking Lot basic features as well as government regulated features.

## Getting Started ##

### Prerequisite ###

To run the application you will need:
* Java 10 or higher
* Maven 3.3 or higher
* Ruby (for functional spec execution)

### Setup ###

Execute:
```bash
$ bin/setup
```

That's all

## Running the Application ##

* With Input File
  ```bash
  $ bin/parking_lot file_inputs.txt
  ```
* Interactive
  ```bash
  $ bin/parking_lot
  ```

## Assumptions & Notes ##
* [Kotlin][1], because it's more succinct than Java, although it generates larger Jar file
* Kotlin encourages multiple class declarations in the same file, [read here][2]. Those are not lack of packaging.
* There is no requirement for structured domains (i.e.: `History`), but it's a nice to have for audit purposes, or perhaps Undo functionality, later.
* Query commands are a little iffy, should find more elegant way of essentially applying Predicates and Projections. Spring Data comes to mind, but it's too cumbersome.
* Provided functional specs is a [misleading][3] at line 39. Encouraging candidates to write more specs but marking it as `pending`.

[1]: https://kotlinlang.org
[2]: https://kotlinlang.org/docs/reference/coding-conventions.html#source-file-organization
[3]: /functional_spec/spec/parking_lot_spec.rb
