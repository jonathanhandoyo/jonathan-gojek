package com.gojek.domain

import com.gojek.command.Command

data class Car(val plate: String, val color: String)
data class Allocation(val position: Int, val car: Car?)
data class ParkingLot(val allocations: List<Allocation>, val lastDispense: Allocation? = null)

data class State(val lot: ParkingLot? = null, val message: String = "")

data class History(val timeline: MutableList<Item>) {
  constructor(state: State = State()): this(mutableListOf(Item(null, state)))
  data class Item(val command: Command? = null, val state: State)

  fun last() = this.timeline.last()
  fun add(command: Command, state: State) = this.timeline.add(Item(command, state))
}
