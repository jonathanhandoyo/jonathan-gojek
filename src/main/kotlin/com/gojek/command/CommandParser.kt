package com.gojek.command

import com.gojek.domain.Car

class CommandParser {
  fun parse(line: String): Command {
    val arguments: List<String> = line.split(" ")

    return when (arguments[0].toLowerCase().trim()) {
      "create_parking_lot" -> Create(arguments[1].toInt())
      "exit" -> Exit
      "leave" -> Leave(arguments[1].toInt())
      "park" -> Park(Car(arguments[1], arguments[2]))
      "registration_numbers_for_cars_with_colour" -> QueryPlateByColour(arguments[1])
      "slot_number_for_registration_number" -> QueryPositionByPlate(arguments[1])
      "slot_numbers_for_cars_with_colour" -> QueryPositionByColour(arguments[1])
      "status" -> Status
      else -> Unknown
    }
  }
}
