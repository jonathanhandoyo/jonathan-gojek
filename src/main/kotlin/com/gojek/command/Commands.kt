package com.gojek.command

import com.gojek.domain.Allocation
import com.gojek.domain.Car
import com.gojek.domain.ParkingLot
import com.gojek.domain.State

sealed class Command {
  abstract fun execute(state: State): State
}

sealed class Query: Command() {
  abstract fun query(state: State): String
  override fun execute(state: State): State {
    return state.copy(
      lot = state.lot?.copy(lastDispense = null),
      message = query(state)
    )
  }
}

data class Create(val size: Int = 1): Command() {
  override fun execute(state: State): State {
    return (1..size)
      .map { Allocation(it, null) }
      .toList()
      .let { State(ParkingLot(it), "Created a parking lot with ${it.size} slots") }
  }
}

object Exit: Command() {
  override fun execute(state: State) = throw IllegalStateException("Bye!")
}

data class Leave(val position: Int): Command() {
  override fun execute(state: State): State {
    val lot = requireNotNull(state.lot)
    val allocations = lot.allocations
    val existing = allocations.find { it.position == position }

    return when (existing == null) {
      true -> state
      else -> {
        val vacant = existing.copy(car = null)
        val modified = allocations - existing + vacant
        state.copy(
          lot = state.lot.copy(allocations = modified, lastDispense = null),
          message = "Slot number ${vacant.position} is free"
        )
      }
    }
  }
}

data class Park(val car: Car): Command() {
  override fun execute(state: State): State {
    val allocations = state.lot!!.allocations
    val vacant = allocations.find { it.car == null }

    return when (vacant != null) {
      true -> {
        val filled = vacant.copy(car = car)
        val modified = allocations - vacant + filled
        state.copy(
          lot = state.lot.copy(allocations = modified, lastDispense = filled),
          message = "Allocated slot number: ${filled.position}"
        )
      }
      else -> {
        state.copy(
          lot = state.lot.copy(lastDispense = null),
          message = "Sorry, parking lot is full"
        )
      }
    }
  }
}

data class QueryPlateByColour(val colour: String): Query() {
  override fun query(state: State): String {
    return state.lot?.allocations
      ?.filter { it.car?.color.equals(colour, ignoreCase = true) }
      ?.map { it.car?.plate }
      ?.joinToString()
      ?: "Not found"
  }
}

data class QueryPositionByColour(val colour: String): Query() {
  override fun query(state: State): String {
    return state.lot?.allocations
      ?.filter { it.car?.color.equals(colour, ignoreCase = true) }
      ?.map { it.position }
      ?.joinToString()
      ?: "Not found"
  }
}

data class QueryPositionByPlate(val plate: String): Query() {
  override fun query(state: State): String {
    return state.lot?.allocations
      ?.find { it.car?.plate.equals(plate, ignoreCase = true) }?.position?.toString()
      ?: "Not found"
  }
}

object Status: Query() {
  override fun query(state: State): String {
    val header = "Slot No.    Registration No    Colour\n"
    return state.lot?.allocations
      ?.filter { it.car != null }
      ?.joinToString("\n", header) { (position, car) ->
        String
          .format(
            "%-11d %-18s %-6s",
            position,
            car?.plate ?: "",
            car?.color ?: ""
          )
          .trim()
      }
      ?: header
  }
}

object Unknown: Command() {
  override fun execute(state: State): State = State(state.lot, "Try again")
}
