package com.gojek

import com.gojek.command.CommandParser
import com.gojek.domain.History
import java.io.File

class ParkingLotApp(
  private val parser: CommandParser = CommandParser(),
  val history: History = History()
) {

  fun withConsole() { while(true) { print("$ "); readLine()!!.also { execute(it) } } }
  fun withInputs(inputs: List<String>) = inputs.forEach { execute(it) }

  private fun execute(input: String) {
    val (_, previousState) = history.last()
    val command = parser.parse(input)
    val modifiedState = command.execute(previousState)

    println(modifiedState.message)
    history.add(command, modifiedState)
  }
}

fun main(args: Array<String>) {
  try {
    when (args.isNotEmpty()) {
      true -> ParkingLotApp().withInputs(File(args[0]).readLines())
      else -> ParkingLotApp().withConsole()
    }
  } catch (e: Exception) {
    println(e.message)
  }
}
