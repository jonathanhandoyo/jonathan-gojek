package com.gojek

import com.gojek.command.*
import com.gojek.domain.Allocation
import com.gojek.domain.Car
import org.junit.jupiter.api.Test

internal class ParkingLotAppTest {

  private val input = """
      create_parking_lot 6
      park KA-01-HH-1234 White
      park KA-01-HH-9999 White
      park KA-01-BB-0001 Black
      park KA-01-HH-7777 Red
      park KA-01-HH-2701 Blue
      park KA-01-HH-3141 Black
      leave 4
      status
      park KA-01-P-333 White
      park DL-12-AA-9999 White
      registration_numbers_for_cars_with_colour White
      slot_numbers_for_cars_with_colour White
      slot_number_for_registration_number KA-01-HH-3141
      slot_number_for_registration_number MH-04-AY-1111
    """.trimIndent()

  private val expected = """

    Created a parking lot with 6 slots
    Allocated slot number: 1
    Allocated slot number: 2
    Allocated slot number: 3
    Allocated slot number: 4
    Allocated slot number: 5
    Allocated slot number: 6
    Slot number 4 is free
    Slot No.    Registration No    Colour
    1           KA-01-HH-1234      White
    2           KA-01-HH-9999      White
    3           KA-01-BB-0001      Black
    5           KA-01-HH-2701      Blue
    6           KA-01-HH-3141      Black
    Allocated slot number: 4
    Sorry, parking lot is full
    KA-01-HH-1234, KA-01-HH-9999, KA-01-P-333
    1, 2, 4
    6
    Not found
    """.trimIndent()

  /**
   * this is a very excessive way of testing, but consider this an e2e
   * i just need to know the whole app created the correct timeline of history
   * this should serve as personal reminder that "bootstrapping tests knows no end"
   */
  @Test
  fun `should start App and create the whole History`() {
    val app = ParkingLotApp().also { it.withInputs(input.lines()) }

    with(app.history) {
      assert(this.timeline.size == 16)

      with(this.timeline[0]) { // <-- start of history
        assert(this.command == null)
        assert(this.state.message == "")
      }

      with(this.timeline[1]) {
        assert(this.command == Create(6))
        assert(this.state.message == "Created a parking lot with 6 slots")
      }

      with(this.timeline[2]) {
        val car = Car("KA-01-HH-1234", "White")
        val allocation = Allocation(1, car)

        assert(this.command == Park(car))
        assert(this.state.message == "Allocated slot number: ${allocation.position}")
        assert(this.state.lot!!.allocations.contains(allocation))
        assert(this.state.lot!!.lastDispense == allocation)
      }

      with(this.timeline[3]) {
        val car = Car("KA-01-HH-9999", "White")
        val allocation = Allocation(2, car)

        assert(this.command == Park(car))
        assert(this.state.message == "Allocated slot number: ${allocation.position}")
        assert(this.state.lot!!.allocations.contains(allocation))
        assert(this.state.lot!!.lastDispense == allocation)
      }

      with(this.timeline[4]) {
        val car = Car("KA-01-BB-0001", "Black")
        val allocation = Allocation(3, car)

        assert(this.command == Park(car))
        assert(this.state.message == "Allocated slot number: ${allocation.position}")
        assert(this.state.lot!!.allocations.contains(allocation))
        assert(this.state.lot!!.lastDispense == allocation)
      }

      with(this.timeline[5]) {
        val car = Car("KA-01-HH-7777", "Red")
        val allocation = Allocation(4, car)

        assert(this.command == Park(car))
        assert(this.state.message == "Allocated slot number: ${allocation.position}")
        assert(this.state.lot!!.allocations.contains(allocation))
        assert(this.state.lot!!.lastDispense == allocation)
      }

      with(this.timeline[6]) {
        val car = Car("KA-01-HH-2701", "Blue")
        val allocation = Allocation(5, car)

        assert(this.command == Park(car))
        assert(this.state.message == "Allocated slot number: ${allocation.position}")
        assert(this.state.lot!!.allocations.contains(allocation))
        assert(this.state.lot!!.lastDispense == allocation)
      }

      with(this.timeline[7]) {
        val car = Car("KA-01-HH-3141", "Black")
        val allocation = Allocation(6, car)

        assert(this.command == Park(car))
        assert(this.state.message == "Allocated slot number: ${allocation.position}")
        assert(this.state.lot!!.allocations.contains(allocation))
        assert(this.state.lot!!.lastDispense == allocation)
      }

      with(this.timeline[8]) {
        val allocation = Allocation(4, null)

        assert(this.command == Leave(allocation.position))
        assert(this.state.message == "Slot number ${allocation.position} is free")
        assert(this.state.lot!!.allocations.contains(allocation))
        assert(this.state.lot!!.lastDispense == null)
      }

      with(this.timeline[9]) {
        assert(this.command == Status)
        assert(this.state.lot!!.lastDispense == null)
        assert(this.state.message == """
          Slot No.    Registration No    Colour
          1           KA-01-HH-1234      White
          2           KA-01-HH-9999      White
          3           KA-01-BB-0001      Black
          5           KA-01-HH-2701      Blue
          6           KA-01-HH-3141      Black
        """.trimIndent())
      }

      with(this.timeline[10]) {
        val car = Car("KA-01-P-333", "White")
        val allocation = Allocation(4, car)

        assert(this.command == Park(car))
        assert(this.state.message == "Allocated slot number: ${allocation.position}")
        assert(this.state.lot!!.allocations.contains(allocation))
        assert(this.state.lot!!.lastDispense == allocation)
      }

      with(this.timeline[11]) {
        val car = Car("DL-12-AA-9999", "White")

        assert(this.command == Park(car))
        assert(this.state.message == "Sorry, parking lot is full")
        assert(this.state.lot!!.lastDispense == null)
      }

      with(this.timeline[12]) {
        val car1 = Car("KA-01-HH-1234", "White")
        val car2 = Car("KA-01-HH-9999", "White")
        val car3 = Car("KA-01-P-333", "White")

        assert(this.command == QueryPlateByColour("White"))
        assert(this.state.message.contains(car1.plate))
        assert(this.state.message.contains(car2.plate))
        assert(this.state.message.contains(car3.plate))
        assert(this.state.lot!!.lastDispense == null)
      }

      with(this.timeline[13]) {
        val allocation1 = Allocation(1, Car("KA-01-HH-1234", "White"))
        val allocation2 = Allocation(1, Car("KA-01-HH-9999", "White"))
        val allocation3 = Allocation(1, Car("KA-01-P-333", "White"))

        assert(this.command == QueryPositionByColour("White"))
        assert(this.state.message.contains(allocation1.position.toString()))
        assert(this.state.message.contains(allocation2.position.toString()))
        assert(this.state.message.contains(allocation3.position.toString()))
        assert(this.state.lot!!.lastDispense == null)
      }

      with(this.timeline[14]) {
        val car = Car("KA-01-HH-3141", "White")
        val allocation = Allocation(6, car)

        assert(this.command == QueryPositionByPlate(car.plate))
        assert(this.state.message == allocation.position.toString())
        assert(this.state.lot!!.lastDispense == null)
      }

      with(this.timeline[15]) {
        assert(this.command == QueryPositionByPlate("MH-04-AY-1111"))
        assert(this.state.message == "Not found")
        assert(this.state.lot!!.lastDispense == null)
      }
    }

    assert(app.history.timeline.joinToString("\n") { it.state.message } == expected)
  }
}
