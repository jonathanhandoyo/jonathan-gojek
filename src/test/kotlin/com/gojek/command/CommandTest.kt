package com.gojek.command

import com.gojek.domain.Car
import com.gojek.domain.State
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class CommandTest {

  @Test
  fun `Create#execute() should create a parking lot`() {
    val actual = State()
      .let { state -> Create(10).execute(state) }

    assert(actual.lot?.allocations?.size == 10)
    assert(actual.message == "Created a parking lot with 10 slots")

    actual.lot?.allocations?.forEach { allocation ->
      assert(allocation.car == null)
    }
  }

  @Test
  fun `Exit#execute() should throw IllegalStateException`() {
    assertThrows<IllegalStateException> { Exit.execute(State()) }
  }

  @Test
  fun `Leave#execute() should vacate the allocation`() {
    val actual = State()
      .let { state -> Create(1).execute(state) }
      .let { state -> Park(Car("KA-01-HH-1234", "White")).execute(state) }
      .let { state -> Leave(1).execute(state) }

    assert(actual.message == "Slot number 1 is free")
    assert(actual.lot?.lastDispense == null)
    assert(actual.lot?.allocations?.find { it.position == 1 }?.car == null)
  }

  @Test
  fun `Park#execute() should dispense first vacant lot`() {
    val actual = State()
      .let { state -> Create(10).execute(state) }
      .let { state -> Park(Car("KA-01-HH-1234", "White")).execute(state) }

    with(actual.lot?.lastDispense) {
      assert(this != null)
      assert(this?.position == 1)
      assert(this?.car == Car("KA-01-HH-1234", "White"))
    }
  }

  @Test
  fun `Park#execute() should not dispense when no vacant lot`() {
    val actual = State()
      .let { state -> Create(1).execute(state) }
      .let { state -> Park(Car("KA-01-HH-1234", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-1234", "White")).execute(state) } // <-- exceed

    assert(actual.message == "Sorry, parking lot is full")
    assert(actual.lot?.lastDispense == null)
  }

  @Test
  fun `QueryPlateByColour#execute() should return Car's plate with colour`() {
    val actual = State()
      .let { state -> Create(6).execute(state) }
      .let { state -> Park(Car("KA-01-HH-1111", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-2222", "Red")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-3333", "Blue")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-4444", "Black")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-5555", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-6666", "Green")).execute(state) }
      .let { state -> QueryPlateByColour("White").execute(state) }

    assert(actual.lot?.lastDispense == null)
    assert(actual.message == "KA-01-HH-1111, KA-01-HH-5555")
  }

  @Test
  fun `QueryPlateByColour#execute() should return Not found when none found`() {
    val actual = State()
      .let { state -> Create(6).execute(state) }
      .let { state -> Park(Car("KA-01-HH-1111", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-2222", "Red")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-3333", "Blue")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-4444", "Black")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-5555", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-6666", "Green")).execute(state) }
      .let { state -> QueryPlateByColour("Silver").execute(state) }

    assert(actual.lot?.lastDispense == null)
    assert(actual.message == "Not found")
  }

  @Test
  fun `QueryPositionByColour#execute() should return Car's position with colour`() {
    val actual = State()
      .let { state -> Create(6).execute(state) }
      .let { state -> Park(Car("KA-01-HH-1111", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-2222", "Red")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-3333", "Blue")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-4444", "Black")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-5555", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-6666", "Green")).execute(state) }
      .let { state -> QueryPositionByColour("White").execute(state) }

    assert(actual.lot?.lastDispense == null)
    assert(actual.message == "1, 5")
  }

  @Test
  fun `QueryPositionByColour#execute() should return Not found when none found`() {
    val actual = State()
      .let { state -> Create(6).execute(state) }
      .let { state -> Park(Car("KA-01-HH-1111", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-2222", "Red")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-3333", "Blue")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-4444", "Black")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-5555", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-6666", "Green")).execute(state) }
      .let { state -> QueryPositionByColour("Silver").execute(state) }

    assert(actual.lot?.lastDispense == null)
    assert(actual.message == "Not found")
  }

  @Test
  fun `QueryPositionByPlate#execute() should return Car's position with colour`() {
    val actual = State()
      .let { state -> Create(6).execute(state) }
      .let { state -> Park(Car("KA-01-HH-1111", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-2222", "Red")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-3333", "Blue")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-4444", "Black")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-5555", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-6666", "Green")).execute(state) }
      .let { state -> QueryPositionByPlate("KA-01-HH-2222").execute(state) }

    assert(actual.lot?.lastDispense == null)
    assert(actual.message == "2")
  }

  @Test
  fun `QueryPositionByPlate#execute() should return Not found when none found`() {
    val actual = State()
      .let { state -> Create(6).execute(state) }
      .let { state -> Park(Car("KA-01-HH-1111", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-2222", "Red")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-3333", "Blue")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-4444", "Black")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-5555", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-6666", "Green")).execute(state) }
      .let { state -> QueryPositionByPlate("KA-01-HH-7777").execute(state) }

    assert(actual.lot?.lastDispense == null)
    assert(actual.message == "Not found")
  }

  @Test
  fun `Status#execute() should print table of allocation`() {
    val actual = State()
      .let { state -> Create(6).execute(state) }
      .let { state -> Park(Car("KA-01-HH-1111", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-2222", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-3333", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-4444", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-5555", "White")).execute(state) }
      .let { state -> Park(Car("KA-01-HH-6666", "White")).execute(state) }
      .let { state -> Leave(3).execute(state) }
      .let { state -> Status.execute(state) }

    assert(actual.lot?.lastDispense == null)
    assert(actual.message == """
      Slot No.    Registration No    Colour
      1           KA-01-HH-1111      White
      2           KA-01-HH-2222      White
      4           KA-01-HH-4444      White
      5           KA-01-HH-5555      White
      6           KA-01-HH-6666      White
    """.trimIndent())
  }

  @Test
  fun `Unknown#execute() should print standard message`() {
    val actual = State()
      .let { state -> Create(6).execute(state) }
      .let { state -> Unknown.execute(state) }

    assert(actual.lot?.lastDispense == null)
    assert(actual.message == "Try again")
  }
}
