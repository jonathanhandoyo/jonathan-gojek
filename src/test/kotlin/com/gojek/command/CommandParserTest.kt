package com.gojek.command

import com.gojek.domain.Car
import org.junit.jupiter.api.Test

internal class CommandParserTest {

  @Test
  fun `should parse to Create command`() {
    assert(CommandParser().parse("create_parking_lot 1") == Create(1))
    assert(CommandParser().parse("create_parking_lot 10") == Create(10))
    assert(CommandParser().parse("create_parking_lot 100") == Create(100))
    assert(CommandParser().parse("create_parking_lot 1000") == Create(1000))
  }

  @Test
  fun `should parse to Exit command`() {
    assert(CommandParser().parse("exit") == Exit)
  }

  @Test
  fun `should parse to Leave command`() {
    assert(CommandParser().parse("leave 1") == Leave(1))
    assert(CommandParser().parse("leave 10") == Leave(10))
    assert(CommandParser().parse("leave 100") == Leave(100))
    assert(CommandParser().parse("leave 1000") == Leave(1000))
  }

  @Test
  fun `should parse to Park command`() {
    assert(CommandParser().parse("park KA-01-HH-1234 White") == Park(Car("KA-01-HH-1234", "White")))
    assert(CommandParser().parse("park KA-01-HH-9999 White") == Park(Car("KA-01-HH-9999", "White")))
    assert(CommandParser().parse("park KA-01-BB-0001 Black") == Park(Car("KA-01-BB-0001", "Black")))
    assert(CommandParser().parse("park KA-01-HH-7777 Red") == Park(Car("KA-01-HH-7777", "Red")))
    assert(CommandParser().parse("park KA-01-HH-2701 Blue") == Park(Car("KA-01-HH-2701", "Blue")))
    assert(CommandParser().parse("park KA-01-HH-3141 Black") == Park(Car("KA-01-HH-3141", "Black")))
  }

  @Test
  fun `should parse to QueryPlateByColour command`() {
    assert(CommandParser().parse("registration_numbers_for_cars_with_colour White") == QueryPlateByColour("White"))
    assert(CommandParser().parse("registration_numbers_for_cars_with_colour Red") == QueryPlateByColour("Red"))
  }

  @Test
  fun `should parse to QueryPositionByColour command`() {
    assert(CommandParser().parse("slot_numbers_for_cars_with_colour White") == QueryPositionByColour("White"))
    assert(CommandParser().parse("slot_numbers_for_cars_with_colour Red") == QueryPositionByColour("Red"))
  }

  @Test
  fun `should parse to QueryPositionByPlate command`() {
    assert(CommandParser().parse("slot_number_for_registration_number KA-01-HH-1111") == QueryPositionByPlate("KA-01-HH-1111"))
    assert(CommandParser().parse("slot_number_for_registration_number KA-01-HH-2222") == QueryPositionByPlate("KA-01-HH-2222"))
  }

  @Test
  fun `should parse to Status command`() {
    assert(CommandParser().parse("status") == Status)
  }

  @Test
  fun `should parse to Unknown command`() {
    assert(CommandParser().parse("unknown") == Unknown)
    assert(CommandParser().parse("gibberish") == Unknown)
    assert(CommandParser().parse("asdflkjas") == Unknown)
    assert(CommandParser().parse("something that should be be recognized") == Unknown)
  }
}
